FROM docker.io/debian:buster-slim

ARG DO_UPGRADE=
ARG KIBANA_VERSION=8.6.0
ARG NODEJS_VERSION=16.18.1
ARG NOTOSANS_TTC=https://github.com/googlefonts/noto-cjk/raw/NotoSansV2.001/NotoSansCJK-Regular.ttc
ENV DEBIAN_FRONTEND=noninteractive \
    KIBANA_GID=1000 \
    KIBANA_HOME=/usr/share/kibana \
    KIBANA_PACKAGE=kibana-${KIBANA_VERSION}-linux-x86_64.tar.gz \
    KIBANA_UID=1000 \
    NODEJS_BASE=https://nodejs.org/dist/v

LABEL io.k8s.description="Kibana" \
      io.k8s.display-name="Kibana $KIBANA_VERSION" \
      io.openshift.expose-services="5601:kibana" \
      io.openshift.tags="kibana" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-kibana" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$KIBANA_VERSION"

COPY config/* /

RUN set -x \
    && mkdir -p $KIBANA_HOME /usr/share/fonts/local /usr/share/man/man1 \
	/usr/share/man/man7 \
    && echo "# Install Dumb-init" \
    && apt-get update -y \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Kibana Dependencies" \
    && apt-get install -y --no-install-recommends ca-certificates curl \
	wget libkrb5-dev git libfontconfig python krb5-config libssl-dev \
	libsasl2-dev libsasl2-modules-gssapi-mit gcc libc-dev make g++ \
	build-essential fontconfig \
    && KBARCH=`uname -m` \
    && if ! echo "$KBARCH" | grep -E '(x86_64|armv6l|armv7l|aarch64)' \
	    >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    fi \
    && curl -fsL -k -L -o /usr/share/fonts/local/NotoSansCJK-Regular.ttc \
	$NOTOSANS_TTC \
    && fc-cache -v \
    && echo "# Install Kibana" \
    && curl -fsL -k -O \
	https://artifacts.elastic.co/downloads/kibana/$KIBANA_PACKAGE \
    && tar -xzf $KIBANA_PACKAGE -C $KIBANA_HOME --strip-components=1 \
    && mv /kibana-docker /usr/local/bin/ \
    && mv /kibana.yml /usr/share/kibana/config/ \
    && groupadd -r kibana -g $KIBANA_GID \
    && useradd -r -s /usr/sbin/nologin -d $KIBANA_HOME \
	-c "Kibana service user" -u $KIBANA_UID -g kibana kibana \
    && mkdir -p /var/log/kibana \
    && if test "$KBARCH" = armv7l -o "$KBARCH" = armv6l; then \
	rm -rf $KIBANA_HOME/node \
	&& curl -fsL -k -O \
	    $NODEJS_BASE$NODEJS_VERSION/node-v$NODEJS_VERSION-linux-armv6l.tar.gz \
	&& tar -xvf node-v$NODEJS_VERSION-linux-armv6l.tar.gz \
	&& mv node-v$NODEJS_VERSION-linux-armv6l $KIBANA_HOME/node \
	&& rm -f node-v$NODEJS_VERSION-linux-armv6l.tar.gz; \
    elif test "$KBARCH" = aarch64; then \
	rm -rf $KIBANA_HOME/node \
	&& curl -fsL -k -O \
	    $NODEJS_BASE$NODEJS_VERSION/node-v$NODEJS_VERSION-linux-arm64.tar.gz \
	&& curl -fsL -k -O \
	    https://us-central1-elastic-kibana-184716.cloudfunctions.net/kibana-ci-proxy-cache/node-re2/uhop/node-re2/releases/download/1.17.4/linux-arm64-93.gz \
        && mv linux-arm64-93.gz /tmp/re2.node.gz \
	&& gunzip -f /tmp/re2.node.gz \
	&& tar -xvf node-v$NODEJS_VERSION-linux-arm64.tar.gz \
	&& mv node-v$NODEJS_VERSION-linux-arm64 $KIBANA_HOME/node \
	&& mv /tmp/re2.node $KIBANA_HOME/node_modules/re2/build/Release/re2.node \
	&& rm -f node-v$NODEJS_VERSION-linux-arm64.tar.gz; \
    fi \
    && echo "# Fixing permissions" \
    && chown -R kibana:root $KIBANA_HOME /var/log/kibana \
    && chmod -R g=u $KIBANA_HOME /var/log/kibana \
    && find / -xdev -perm -4000 -exec chmod u-s {} \; \
    && echo "# Cleaning Up" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/man $KIBANA_PACKAGE \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER $KIBANA_UID
WORKDIR $KIBANA_HOME
ENTRYPOINT ["dumb-init","--","/usr/local/bin/kibana-docker"]
